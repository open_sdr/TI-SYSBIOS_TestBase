/*
 * test_common.h
 *
 *  Created on: 22 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_TEST_COMMON_H_
#define SYS_TEST_COMMON_H_

#include <SDR/host_interface/io.h>

#ifdef __cplusplus
extern "C"{
#endif

HOST_IO_t * HOST_IO();

#ifdef __cplusplus
}
#endif

#endif /* SYS_TEST_COMMON_H_ */
