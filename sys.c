/*
 * control.c
 *
 *  Created on: 17 янв. 2018 г.
 *      Author: svetozar
 */

#include <sys_log_cfg.h>
#define LOG_NAME "SYSTEM"
#define LOG_LEVEL SYSTEM_LOG_LEVEL

#include "log.h"
#include "control.h"
#include "timestamp.h"

#include <ti/board/board.h>
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/Types.h>

#define USE_CLOCK_TICK_PERIOD
#ifdef USE_CLOCK_TICK_PERIOD
#include <ti/sysbios/knl/Clock.h>
#else
#include <xdc/runtime/Timestamp.h>
#endif

#include "HostInterface.h"

static HostInterface_t HostInterface;

HOST_IO_t * HOST_IO()
{
  return &HostInterface.io;
}

#ifndef SYS_BOARD_INIT_CFG
#define SYS_BOARD_INIT_CFG ( BOARD_INIT_MODULE_CLOCK | BOARD_INIT_PINMUX_CONFIG )
#endif

void init_system()
{
  LOG_RAW("\n"
      LOG_SPACES_FOR_LVL"==========================\n"
      LOG_SPACES_FOR_LVL"      (enter point)\n"
      LOG_SPACES_FOR_LVL"==========================\n");

  LOG_FXN_TRACE(START);

#ifdef SYS_BOARD_INIT_CFG
  {
    Board_STATUS boardStatus = Board_init(SYS_BOARD_INIT_CFG);
    if (boardStatus != BOARD_SOK)
    {
      LOG(FATAL,"init Board failure");
      BIOS_exit(0);
    }
  }
#endif

  if (!init_HostInterface(&HostInterface))
  {
    LOG(FATAL,"init HOST interface failure");
    BIOS_exit(0);
  }

  LOG_SUF(INFO,INIT);
}

int system_exec()
{
  LOG_RAW("");
  LOG_SUF(TRACE,RUNNING);
  LOG_RAW(LOG_SPACES_FOR_LVL"--------------------------\n");

  init_timestamp_system();
  BIOS_start();    /* does not return */
  return(0);
}

void system_exit(int code)
{
  LOG_SUF(INFO,FINISH);
  BIOS_exit(code);
}

UInt32 system_cpu_freq()
{
  Types_FreqHz freq;
  BIOS_getCpuFreq(&freq);
  SYS_ASSERT(!freq.hi);
  return freq.lo;
}

UInt32 system_ticks_count_ms(Int32 timeout_ms)
{
#ifdef USE_CLOCK_TICK_PERIOD
  return (1000000/Clock_tickPeriod*timeout_ms)/1000;
#else
  Types_FreqHz freq;
  Timestamp_getFreq(&freq);
  SYS_ASSERT(!freq.hi);
  return ((freq.lo)*timeout_ms)/1000;
#endif
}
