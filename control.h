/*
 * control.h
 *
 *  Created on: 17 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_CONTROL_H_
#define SYS_CONTROL_H_

#include "common.h"

#ifdef __cplusplus
extern "C"{
#endif

void init_system();
int system_exec();
void system_exit(int code);

#ifdef __cplusplus
}
#endif

#endif /* SYS_CONTROL_H_ */
