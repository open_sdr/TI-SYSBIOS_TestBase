/*
 * timestamp.h
 *
 *  Created on: 3 окт. 2018 г.
 *      Author: svetozar
 */

#ifndef SYSTEM_ELAPSEDTIMER_H_
#define SYSTEM_ELAPSEDTIMER_H_

#include "timestamp.h"
#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  timestamp_t _st_begin, _st_overhead;
} ElapsedTimer_t;

static inline void init_ElapsedTimer(ElapsedTimer_t * This)
{
  init_timestamp_system();
  This->_st_begin = timestamp_clk();
  timestamp_t _st_end = timestamp_clk();
  This->_st_overhead = _st_end - This->_st_begin;
  This->_st_begin = -1;
}

static inline void ElapsedTimer_start(ElapsedTimer_t * This)
{
  This->_st_begin = timestamp_clk();
}

#define __tmp_ELAPSED_US(This) ( ((float)(_st_end - This->_st_begin - This->_st_overhead))*1000000/system_cpu_freq() )

static inline float ElapsedTimer_elapsed_us(ElapsedTimer_t * This)
{
  timestamp_t _st_end = timestamp_clk();
  float res_us = __tmp_ELAPSED_US(This);
  return res_us;
}

static inline float ElapsedTimer_restart(ElapsedTimer_t * This)
{
  timestamp_t _st_end = timestamp_clk();
  float res_us;
  if (This->_st_begin >= 0)
    res_us = __tmp_ELAPSED_US(This);
  else
    res_us = 0;
  This->_st_begin = _st_end;
  return res_us;
}

#undef __tmp_ELAPSED_US

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_ELAPSEDTIMER_H_ */
